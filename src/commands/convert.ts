import {Command, flags} from "@oclif/command";
import {existsSync, writeFileSync} from "fs";
import csv from "csvtojson";

export interface CsvRow {
  measurement: string;
  datentyp: string;
  namespaceIndex: string;
  identifierTyp?: string;
  identifier: string;
  anlage: string;
  kunde: string;
  topic: string;
  name: string;
  factor: string;
  precision: string;
}

export default class Convert extends Command {
  static description = "convert OpcuaReadVars.csv to vars.json";

  static examples = [
    "$ dingx-iot convert .",
    "$ dingx-iot convert -d ./export .",
  ];

  static flags = {
    help: flags.help({char: "h"}),
    destination: flags.string({char: "d", description: "export path"}),
  };

  static args = [{name: "file"}];

  async run() {
    const {args, flags} = this.parse(Convert);

    const destination = flags.destination ?? ".";
    const csvFilePath = args.file;
    if (!csvFilePath) throw new Error("A");
    if (!existsSync(csvFilePath)) throw new Error("file not found");
    csv({trim: true, delimiter: ";"})
      .fromFile(csvFilePath)
      .then((json: CsvRow[]) => {
        const nodes = json.map(row => {
          const temp = {
            ...row,
            namespaceIndex: row.namespaceIndex,
            identifierType: row.identifierTyp,
            identifier: row.identifier,
          };
          delete temp.identifierTyp;
          return temp;
        });
        this.log(`create ${destination}/vars.json`);
        writeFileSync(
          `${destination}/vars.json`,
          Buffer.from(JSON.stringify(nodes, null, 2))
        );
      });
  }
}
