dingx-iot-cli
=============



[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/dingx-iot-cli.svg)](https://npmjs.org/package/@north-tec/dingx-iot-cli)
[![Downloads/week](https://img.shields.io/npm/dw/dingx-iot-cli.svg)](https://npmjs.org/package/dingx-iot-cli)
[![License](https://img.shields.io/npm/l/dingx-iot-cli.svg)](https://github.com///blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g @north-tec/dingx-iot-cli
$ dingx-iot COMMAND
running command...
$ dingx-iot (-v|--version|version)
@north-tec/dingx-iot-cli/1.0.2 win32-x64 node-v14.16.0
$ dingx-iot --help [COMMAND]
USAGE
  $ dingx-iot COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`dingx-iot convert [FILE]`](#dingx-iot-convert-file)
* [`dingx-iot help [COMMAND]`](#dingx-iot-help-command)

## `dingx-iot convert [FILE]`

convert OpcuaReadVars.csv to vars.json

```
USAGE
  $ dingx-iot convert [FILE]

OPTIONS
  -d, --destination=destination  export path
  -h, --help                     show CLI help

EXAMPLES
  $ dingx-iot convert .
  $ dingx-iot convert -d ./export .
```

_See code: [src/commands/convert.ts](https://gitlab.com/Shingazu/dingx-iot-cli/blob/v1.0.2/src/commands/convert.ts)_

## `dingx-iot help [COMMAND]`

display help for dingx-iot

```
USAGE
  $ dingx-iot help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v3.2.2/src/commands/help.ts)_
<!-- commandsstop -->
