import {expect, test} from "@oclif/test";

describe("hello", () => {
  test
    .stdout()
    .command(["convert"])
    .it("runs convert -h", ctx => {
      expect(ctx.stdout).to.contain("USAGE");
    });
});
